provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "my_instance" {
  ami           = "ami-0440d3b780d96b29d" 
  instance_type = "t2.micro"              
  key_name      = "ec2key" 
  tags = {
    Name = "my_instance"
  }
  connection {
    type        = "winrm"
    user        = "Administrator"
    password = file("assign4.ppk")
    host        = self.public_ip
    timeout     = "5m" 
  }

  provisioner "local-exec" {
    command = "puppet apply ../puppet/site.pp"
  }
}
output "public_ip" {
  value = aws_instance.my_instance
}
